# Pubmed Central Client

## Getting Started

### Prerequisites

* [git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
* [python 3](https://docs.python.org/3/using/index.html)

### Installing

Clone project:


```
git clone https://nsalikov@bitbucket.org/nsalikov/pmcdb.git
```

Change directory and install requirements:

```
cd pmcdb
pip install -r requirements.txt
```

If you have both python 2 and python 3 installed, use pip3 instead:

```
cd pmcdb
pip3 install -r requirements.txt
```

### Configuration

Set up directories for epubs and extracts.
```
DB_DIR = 'E:\\projects\\upwork\\pmcdb\\epubs'
EX_DIR = 'E:\\projects\\upwork\\pmcdb\\extracts'
```

If you need to use a proxy, you can set up `PROXIES` dictionary. You can also configure proxies by setting the environment variables HTTP_PROXY and HTTPS_PROXY (check python-requests.org link below).

```
# http://docs.python-requests.org/en/master/user/advanced/
# Ctrl+F: proxies

PROXIES = {
    # 'http': 'http://10.10.1.10:3128',
    # 'https': 'http://10.10.1.10:1080',
}
```

You can tweak html and text output by adding or removing selectors in `SELECTORS_TO_REMOVE`.

```
SELECTORS_TO_REMOVE = [
        'head',
        'body div[class*=banner]',
        'body div.meta div.citation',
        'body div.meta div.aff',
        'body div.meta div.author-notes',
        'body div.meta div.meta-permissions',
        'body div.meta div.article-id',
        'body div.meta div.pub-date',
        'body div.body div.clearfix',
        'body div.back',
]

```

## Usage

You can use it ether as command-line script or as interactive shell:

```
Usage: pmcdb.py [OPTIONS] COMMAND [ARGS]...

Options:
  --help  Show this message and exit.

Commands:
  extract  Extract text from local epubs.
  get      Get epubs from PMC by IDs.
  index    Show index of local epubs.
  repl     Start an interactive shell.
  show     Show local epubs.
```

Firstly, prepare list of PMC's ID, then get those IDs from server:

```
python pmcdb.py get 4221732 3348509
# you can also specify file with IDs (one ID per line):
python pmcdb.py get -f ids.txt
# or to combine both way:
python pmcdb.py get -f ids.txt 5583211
```

`Show` and `index` will help you to check files and their metadata accordingly.

To extract text you should use `extract` command.

```
python pmcdb.py extract 4221732 3348509
# or
python pmcdb.py extract -f ids.txt
# or
python pmcdb.py extract -f ids.txt 5583211
```