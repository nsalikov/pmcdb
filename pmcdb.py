# -*- coding: utf-8 -*-

import os
import re
import csv
import time
import requests

from bs4 import BeautifulSoup

from tabulate import tabulate

import ebooklib
from ebooklib import epub

import click
from click_repl import register_repl

#######################################################
# Configuration
#######################################################

DB_DIR = 'E:\\projects\\upwork\\pmcdb\\epubs'
EX_DIR = 'E:\\projects\\upwork\\pmcdb\\extracts'

# http://docs.python-requests.org/en/master/user/advanced/
# Ctrl+F: proxies

PROXIES = {
    # 'http': 'http://10.10.1.10:3128',
    # 'https': 'http://10.10.1.10:1080',
}

HEADERS = {'user-agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:55.0) Gecko/20100101 Firefox/55.0'}

URL = 'https://www.ncbi.nlm.nih.gov/pmc/articles/PMC{id}/epub/'

RE_TO_REMOVE = '\s*\[[^\[\]]+?href=[\"\']#[^\[\]]+?\]|\s*\([^()]+?href=[\"\']#[^FfTt][^()]+?\)'

SELECTORS_TO_REMOVE = [
        'head',
        'body div[class*=banner]',
        'body div.meta div.citation',
        'body div.meta div.aff',
        'body div.meta div.author-notes',
        'body div.meta div.meta-permissions',
        'body div.meta div.article-id',
        'body div.meta div.pub-date',
        'body div.body div.clearfix',
        'body div.back',
]

#######################################################
# Commands
#######################################################

@click.group()
def cli():
    pass


@cli.command()
def show():
    """Show local epubs."""

    epubs = get_epub_filenames()

    if not epubs:
        click.echo('No epub files in database')
        return

    for name in epubs:
        click.echo(name)


@cli.command()
def repl():
    prompt_kwargs = {
        'message': 'pmcdb> ',
    }
    repl(click.get_current_context(), prompt_kwargs=prompt_kwargs)


@cli.command()
@click.option('fh', '--file', '-f', type=click.File('r'), help='Optional file with PMC\'s IDs')
@click.argument('ids', nargs=-1)
def get(fh, ids):
    """Get epubs from PMC by IDs."""

    ids = load_ids(fh, ids)

    if not ids:
        ctx = click.get_current_context()
        print(ctx.get_help())
        return

    for id in ids:
        url = URL.format(id=id)
        click.echo(url + '\t\t\t', nl=False)
        r = requests.get(url, headers=HEADERS, proxies=PROXIES)
        click.echo(r.status_code)

        time.sleep(1)

        if r.status_code != 200:
            continue

        path = os.path.join(DB_DIR, id + '.epub')
        with open(path, 'wb') as fout:
            fout.write(r.content)


@cli.command()
@click.option('fh', '--file', '-f', type=click.File('r'), help='Optional file with PMC\'s IDs')
@click.argument('ids', nargs=-1)
def extract(fh, ids):
    """Extract text from local epubs."""

    ids = load_ids(fh, ids)

    if not ids:
        ctx = click.get_current_context()
        print(ctx.get_help())
        return

    for id in ids:
        epub_path = os.path.join(DB_DIR, id + '.epub')
        click.echo('Extracting\t\t\t{}'.format(epub_path))
        book = epub.read_epub(epub_path)

        content = None
        for item in book.get_items_of_type(ebooklib.ITEM_DOCUMENT):
            # click.echo(item.get_name())
            # click.echo(item.get_id())
            if 'idm' in item.get_id():
                content = item.get_content().decode()

        result = clean(content)

        html_path = os.path.join(EX_DIR, id + '.html')
        with open(html_path, 'w', encoding='utf-8') as fout:
            fout.write(result['html'])

        text_path = os.path.join(EX_DIR, id + '.txt')
        with open(text_path, 'w', encoding='utf-8') as fout:
            fout.write(result['text'])


@cli.command()
def index():
    """Show index of local epubs."""
    filenames = get_epub_filenames()
    metadata = []

    # http://ebooklib.readthedocs.io/en/latest/

    for f in filenames:
        epub_path = os.path.join(DB_DIR, f)
        book = epub.read_epub(epub_path)

        title = book.get_metadata('http://purl.org/dc/elements/1.1/', 'title')
        title = title[0][0] if title else None

        contributors = []
        for con in book.get_metadata('http://purl.org/dc/elements/1.1/', 'contributor'):
            con = con[0] if con else None
            contributors.append(con)
        contributors = ', '.join(contributors)

        date = book.get_metadata('http://purl.org/dc/elements/1.1/', 'date')
        date = date[0][0] if date else None

        description = book.get_metadata('http://purl.org/dc/elements/1.1/', 'subject')
        description = description[0][0] if description else None

        id = f.replace('.epub', '')
        meta = {'id': id, 'date': date, 'title': title, 'contributors': contributors, 'description': description}
        metadata.append(meta)

    rows = [{'id': meta['id'], 'date': meta['date'], 'title': meta['title']} for meta in metadata]

    click.echo_via_pager(tabulate(rows, headers='keys'))

#######################################################
# Functions
#######################################################

def clean(content):
    content = re.sub(RE_TO_REMOVE, '', content)

    soup = BeautifulSoup(content, 'lxml')

    for selector in SELECTORS_TO_REMOVE:
        for tag in soup.select(selector):
            tag.extract()

    for sup in soup.find_all('sup'):
        for link in sup.find_all('a'):
            if link.get('href').startswith('#'):
                sup.extract()
                break

    return {'text': re.sub('\n+', '\n\n', soup.get_text()).strip(), 'html': soup.prettify(formatter=None)}


def load_ids(fh, ids):
    ids = list(ids)

    if fh:
        for line in fh:
            ids.append(line.strip())

    ids = [re.sub(r'\D+', '', id) for id in ids]

    return list(set(filter(None, ids)))


def get_epub_filenames():
    if not os.path.isdir(DB_DIR) or not os.path.exists(DB_DIR):
        click.echo('Could not found database\'s directory')
        return

    return [name for name in next(os.walk(DB_DIR))[2] if name.endswith('.epub')]

#######################################################
# Main
#######################################################

if __name__ == '__main__':
    register_repl(cli)
    cli()
